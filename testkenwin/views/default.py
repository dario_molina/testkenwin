from pyramid.httpexceptions import HTTPFound
from pyramid.security import remember
from pyramid.view import view_config

from testkenwin.models.mymodel import User


@view_config(route_name='login', renderer='../templates/login.jinja2')
def login(request):
    next_url = request.params.get('next', request.referrer)
    if not next_url:
        next_url = request.route_url('home')
    message = ''
    username = ''
    if 'form.submitted' in request.params:
        username = request.params['username']
        password = request.params['password']
        user = request.dbsession.query(User).filter_by(username=username).first()
        if user is not None and user.verify_password(password):
            headers = remember(request, user.id)
            return HTTPFound(location=next_url, headers=headers)
        message = 'Failed login'
    return dict(
        message=message,
        url=request.route_url('login'),
        next_url=next_url,
        username=username,
    )


@view_config(route_name='home', renderer='../templates/home.jinja2')
def home(request):
    return dict(
        user=request.user.username
    )
