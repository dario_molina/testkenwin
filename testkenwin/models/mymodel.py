from sqlalchemy import Column, Integer, Unicode

from .meta import Base


class User(Base):
    __tablename__ = 'User'
    id = Column(Integer, primary_key=True)
    username = Column(Unicode(255), nullable=False)
    password = Column(Unicode(255), nullable=False)

    def verify_password(self, password):
        return self.password == password
