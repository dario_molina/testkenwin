# Instalación del Proyecto


#### 1. Instalar postgres
    $ sudo apt-get install libpq-dev
    $ sudo apt-get install postgresql

#### 2. Instalar algún ambiente virtual y activarlo con python 3.6.7 e instalar las dependencias
En caso de usar virtualenvwrapper

    $ mkvirtualenv --python=python3 kenwin
    $ workon kenwin
    $ pip install -e .

#### 3. Crear la base de datos postgres, usuario y password. Luego realizar el restore de la db

    $ su - postgres
    $ createuser kenwin
    $ createdb testkenwin
    $ psql testkenwin

Una vez ingresado a la db testkenwin, escribir en la consola:

    $ \password

Eso pedirá ingresar un password el cuál deberá ser 'kenwin123'

    $ ALTER USER kenwin WITH PASSWORD 'password';
    $ ALTER DATABASE testkenwin OWNER TO kenwin;	
    $ GRANT ALL PRIVILEGES ON DATABASE testkenwin TO kenwin;

con el siguiente comando:

    $ \conninfo

podemos ver el puerto y en caso de NO COINCIDIR con el de configuración, editarlo en el archivo development.ini, en la variable sqlalchemy.url

    $ \q
    $ exit

Para realizar el restore de la base de datos, realizamos lo siguiente. Dependiendo de la ruta donde se clonó el proyecto testkenwin, buscamos el path de la db llamada testkenwin.bak en la carpeta task-descrption .

    $ psql testkenwin < ~/Proyectos/testkenwin/task-description/testkenwin.bak

#### 4. Se debe tener ambiente virtual activado para correr el servidor de la siguiente manera.
   
    $ pserve development.ini

Se debe correr a la altura del archivo development.ini

#### 5. Abrir un navegador web
Ingresar la siguiente ruta en un navegador web

    $ http://localhost:6543

Esto nos llevará a una vista de login.

Las credenciales para el ingreso son las siguientes

### username = kenwin
### password = kenwin123


